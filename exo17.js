function reversibleInclusiveList(start, end) {
    var arr = [];
    let j = 0;
    if (start < end) {
        for (let i = start; i <= end; i++) {
            arr[j] = i;
            j++;
        }
    }
    if (end < start) {
        for (let i = start; i >= end; i--) {
            arr[j] = i;
            j++;
        }
    }
    return arr;
}
var resultat = reversibleInclusiveList(7,2);
console.log(resultat);