class Rectangle {
    constructor(sideA, sideB) {
      this.sideA = sideA
      this.sideB = sideB
    }
    getArea(){return this.sideA*this.sideB}
    getPerimeter(){return (this.sideA + this.sideB) *2}
  }
  
  let PI= 3.14;
  class Circle {
      // put your code here
      constructor(reyon){
          this.reyon = reyon
      }
  
      getArea(){
          return this.reyon*this.reyon *PI;
      }
      getPerimeter(){
          return  2*PI*this.reyon;
      }
      
  };
     /* function round(number) {
    var factor = Math.pow(10, 5);
    return Math.round(number * factor) / factor;
      }*/
  
  // unquote and use run to test these cases:
  
  let q = new Circle(20);
  console.log(q.getArea());
  console.log(q.getPerimeter());