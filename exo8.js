drinks = [
  { name: 'lemonade', price: 90 },
  { name: 'peach', price: 23 },
  { name: 'lime', price: 432 }
]
function sortDrinkByPrice(drinks) {
  // sort by value
  const sorted=drinks.sort((a, b) => {
    if (a.price < b.price) { return -1; }
    else { return 1; }
  });
  return sorted;
}
var resultat= sortDrinkByPrice(drinks);
 console.log(drinks);
 console.log(resultat);
/* solution 2 */
/*function sortDrinkByPrice(drinks){
  const sorted = drinks.sort(function(a, b){
    return a.price - b.price;
  });
  return sorted;
}*/
//console.log(sortDrinkByPrice(drinks));