function getBudgets(arr) {
	let temp=0;
	for(let i=0; i<arr.length;i++){
		temp+= arr[i].budget;
	}
	return temp;
}
let resultat = getBudgets([
    { name: "John", age: 21, budget: 23000 },
    { name: "Steve",  age: 32, budget: 40000 },
    { name: "Martin",  age: 16, budget: 2700 }
  ]);

console.log(resultat);