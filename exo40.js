function daysUntil2021(date) {
	let date1 = new Date(date);
	let date2 = new Date('12/31/2020');
	let result = parseInt((date2 - date1) / (1000 * 60 * 60 * 24), 10) +1;
	return  `${result} ${result == 1 ? 'day':'days'}` 
}
