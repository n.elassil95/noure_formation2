function reverseWords(string) {
	return string.split(' ').reverse().join(' ')
}
let q = reverseWords("the sky is blue");
console.log(q);