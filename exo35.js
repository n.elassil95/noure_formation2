function makePlusFunction(baseNum) {
    return function(x){
       return baseNum + x;
   };
}
const plusFive = makePlusFunction(5);
let resultat = plusFive(5);
console.log(resultat);