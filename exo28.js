function getTotalPrice(groceries) {
	let temp=0;
	for(let i=0; i<groceries.length;i++){
		temp+= groceries[i].price *groceries[i].quantity;
	}
	return Math.round(temp* 10) / 10;
	
}
let resultat = getTotalPrice([
	{ product: "Milk", quantity: 1, price: 1.50 },
	{ product: "Eggs", quantity: 12, price: 0.10 },
	{ product: "Bread", quantity: 2, price: 1.60 },
	{ product: "Cheese", quantity: 1, price: 4.50 }
]);

console.log(resultat);